def vector(pt1, pt2):
	return Point3(pt2.x - pt1.x, pt2.y - pt1.y, pt2.z - pt1.z)

def vector_length(v):
	return math.sqrt(dot_product(v, v))

def center(pt1, pt2):
	return Point3((pt1.x + pt2.x)/2, (pt1.y + pt2.y)/2, (pt1.z + pt2.z)/2)

def debug_point(point, size = 10):
	sphere = scene.createSphere(size, 16, 16, True)
	scene.setLocalMatrix(sphere, geom.fromTRS(point, pxz.geom.Point3(0,0,0), pxz.geom.Point3(1,1,1)))
	scene.setParent(sphere, scene.getRoot())

def bucket(root, n):
	aabb = scene.getAABB([root])
	origin = aabb.low
	high = aabb.high
	vector_aabb = vector(origin, high)
	
	step_x = int(vector_aabb.x / n)
	step_y = int(vector_aabb.y / n)
	step_z = int(vector_aabb.z / n)
	
	parts = scene.getPartOccurrences(root)
	chunks_parts = [[[list() for k in range(n)] for j in range(n)] for i in range(n)]
	print(chunks_parts)
	
	for part in parts:
		part_aabb = scene.getAABB([part]) # costly
		center_part = center(part_aabb.high, part_aabb.low)
		origin_to_center = vector(origin, center_part)
		x = int(origin_to_center.x / step_x)
		y = int(origin_to_center.y / step_y)
		z = int(origin_to_center.z / step_z)
		chunks_parts[x][y][z].append(part)
	return chunks_parts

#chunks = bucket(1, 4)
print(chunks)
scene.clearSelection()
scene.select(chunks[3][1][1])


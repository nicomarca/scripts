# merge by name
name = 'W012'
occurrences = scene.findByProperty('Name', name)
by_assemblies = dict()

 

for occurrence in occurrences:
    parent = scene.getParent(occurrence)
    if parent in by_assemblies.keys():
        by_assemblies[parent].append(occurrence)
    else:
        by_assemblies[parent] = [occurrence]

 

for key, value in by_assemblies.items():
    for occurrence in value:
        algo.createNormals([occurrence])
        algo.deleteLines([occurrence])
        algo.crackEdges([occurrence])
        algo.explodeConnectedMeshes([occurrence])
        triangles = dict()
        min_1 = 9999999999
        patch_1 = 0
        min_2 = 9999999999
        patch_2 = 0
        for patch in scene.getChildren(occurrence):
            polycount = scene.getPolygonCount([patch])
            if polycount < min_1:
                min_1 = polycount
                patch_1 = patch
                continue
            if polycount < min_2:
                min_2 = polycount
                patch_2 = patch
                continue
        if patch_1 != 0: scene.deleteOccurrences([patch_1])
        if patch_2 != 0: scene.deleteOccurrences([patch_2])
        algo.repairMesh([occurrence], 0.1, True, False)
    value = scene.mergeParts(value)
    algo.repairMesh(value, 0.5, True, False)
    algo.extractNeutralAxis(value, 100.000000)
    algo.explodeConnectedMeshes(value)
    algo.sweep(value, 4.000000, 5, True, False, True)
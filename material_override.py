core.setInteractiveMode(True)

# import a checker image
imgChecker = material.importImage("C:/Users/cedric/Pictures/UVTextureChecker1024.png")

# create an unlit material an set the checker image on it
matChecker = material.createMaterial("checker", "unlittexture")
uvSet = core.choose("Choose an uvSet", ["0", "1", "2"], 0)
textureDef = "[[1.0,1.0], [0.0,0.0], " + str(imgChecker) + ", " + str(uvSet) + "]" 
core.setProperty(matChecker, "unlittexture", textureDef)

# enable override material with the checker material
view.enableOverrideMaterial(matChecker)
view.refreshViewer(forceUpdate=True)

core.askYesNo("Happy ?")

# disable override material
view.disableOverrideMaterial()
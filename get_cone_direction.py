
import math
import sys

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # Functions to create a sphere occurrence
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def createPartOccurrence(name, surface, parent, loop=[]):
	occurrence = scene.createOccurrence(name, parent)
	part = scene.addComponent(occurrence, scene.ComponentType.Part)
	model = cad.createModel()
	scene.setPartModel(part, model)
	face = cad.createFace(surface, loop)
	cad.addToModel(face, model)
	return occurrence

def assignMaterialWithRGB(occurrences, rgb):
	mat = material.createMaterial(str(rgb), 'color')
	core.setProperty(mat, 'color', str(rgb))
	for oc in occurrences:
		core.setProperty(oc, 'Material', str(mat))
	
def createSphere(size, pos, name, color, parent):
	sphere = cad.createSphereSurface(size)
	plane = cad.createPlaneSurface()
	# create trim curves
	curves = list()
	curves.append(cad.createSurfacicCurve(plane, cad.createSegmentCurve([0, 0, 0], [0, size*4, 0])))

	edges = list()
	for curve in curves: edges.append(cad.createEdgeFromCurve(curve))
	loop = cad.createLoop(edges, [True for edge in edges])
	
	occurrence = scene.createOccurrence(name)
	scene.setParent(occurrence, parent)
	
	transform = [[1.0000, 0.0000, 0.0000, pos[0]],\
							 [0.0000, 1.0000, 0.0000, pos[1]],\
							 [0.0000, 0.0000, 1.0000, pos[2]],\
							 [0.0000, 0.0000, 0.0000, 1.000]]
	scene.applyTransformation(occurrence, transform)

	sphereOccurrence = createPartOccurrence('sphere', sphere, occurrence)
	algo.tessellate([sphereOccurrence], 0.05, -1, -1)
	assignMaterialWithRGB([sphereOccurrence], color)
	return occurrence

def debugPoint(point, name, color=[255, 255, 255, 1]):
	sphere = createSphere(2, point, name, color, scene.getRoot())	
	return sphere
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def point_x(pt):
	return pt[0]
	
def point_y(pt):
	return pt[1]

def point_z(pt):
	return pt[2]

def dot_product(pt1, pt2):
	return point_x(pt1)*point_x(pt2) + point_y(pt1)*point_y(pt2) + point_z(pt1)*point_z(pt2)

def vector_length(v):
	return math.sqrt(dot_product(v,v))

def vector(a, b):
	return [b_ - a_ for b_, a_ in zip(b, a)]

def getVertices(root):
	
	meshes = list()
	for occurrence in scene.getPartOccurrences(root):
		part = scene.getComponent(occurrence, pxz.scene.ComponentType.Part)
		mesh = scene.getPartMesh(part)
		meshes.append(mesh)

	mesh_definitions = polygonal.getMeshDefinitions(meshes)
	vertices = list()
	for mesh_def in mesh_definitions:
		vertices += mesh_def[1]
	
	return vertices

def getOrigin(points):
	min_x = sys.maxsize
	min_y = sys.maxsize
	min_z = sys.maxsize
	max_x = -sys.maxsize
	max_y = -sys.maxsize
	max_z = -sys.maxsize
	
	for point in points:
		if point_x(point) < min_x:
			min_x = point_x(point)
		elif point_y(point) < min_y:
			min_y = point_y(point)
		elif point_z(point) < min_z:
			min_z = point_z(point)
		elif point_x(point) > max_x:
			max_x = point_x(point)
		elif point_y(point) > max_y:
			max_y = point_y(point)
		elif point_z(point) > max_z:
			max_z = point_z(point)
			
	return [(max_x + min_x)/2, (max_y + min_y)/2, (max_z + min_z)/2]

def findLeSensDeLaVis(occurrence):

	vertices = getVertices(occurrence)

	# Get oriented bounding box
	obb = scene.getOBB([occurrence])
	
	# Get max length
	lengths = [vector_length(x) for x in obb[-3:]]

	direction = lengths.index(max(lengths))
	
	o = getOrigin(vertices)
	o[direction] += 1500 # TODO: change it with a size factor
	
	# Get I
	i = o.copy()
	i[direction] -= 1500 # TODO: change it with a size factor
	
	oi = vector(o, i)

	# Get points to project on OI
	minValue = sys.maxsize
	maxValue = -sys.maxsize
	minPoint, maxPoint = list(), list()
	
	for p in vertices:

		# O + dot(OP, OI) / dot(OI, OI) * OI
		op =  vector(o, p)
		factor = dot_product(op, oi) / dot_product(oi, oi)
		q = [a + b for a, b in zip(o, [factor * x for x in oi])]
		
		pq = vector(p, q)
		distance = vector_length(pq)
		
		if distance < minValue:
			minValue = distance
			minPoint = q
		if distance > maxValue:
			maxValue = distance
			maxPoint = q
	
	return minPoint, maxPoint

# occurrence must be the conic part
occurrence = 8

# returns local coordinates
minPoint, maxPoint = findLeSensDeLaVis(occurrence)

# place a blue sphere on min point and a red one on max 
globalMatrix = scene.getGlobalMatrix(occurrence)

sphere = debugPoint(minPoint, 'Min', [0, 0, 255, 1])
scene.applyTransformation(sphere, globalMatrix)

sphere = debugPoint(maxPoint, 'Max', [255, 0, 0, 1])
scene.applyTransformation(sphere, globalMatrix)


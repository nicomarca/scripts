def assignMaterialsFromMetadata():
	existingMaterials = {core.getProperty(mat, 'Name'):mat for mat in material.getAllMaterials()}
	filteredOccurrences = scene.findByMetadata('CAD_MATERIAL', '.*')
	filteredMetadata = scene.getComponentByOccurrence(filteredOccurrences, 5, True) # get all metadata components containing the 'CAD_MATERIAL' property
	for occurrence, metadata in zip(filteredOccurrences, filteredMetadata):
		materialName = scene.getMetadata(metadata, 'CAD_MATERIAL')
		if materialName in existingMaterials.keys():
			core.setProperty(occurrence, 'Material', str(existingMaterials[materialName]))
		else:
			newMaterial = material.createMaterial(materialName, 'color') # pattern can be 'standard', 'color', 'PBR', or 'unlittexture'
			core.setProperty(newMaterial, 'color', str([0.5, 0.5, 0.5, 1])) # color values could be in another property
			core.setProperty(occurrence, 'Material', str(newMaterial))
			existingMaterials[materialName] = newMaterial
			
assignMaterialsFromMetadata()
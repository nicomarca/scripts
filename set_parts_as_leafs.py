
root = 1

parts = scene.getPartOccurrences(root)
core.configureInterfaceLogger(False, False, False)
core.pushProgression(len(parts))
for part in parts:
	core.stepProgression()
	children = scene.getChildren(part)
	if len(children) > 0:
		newParent = scene.createOccurrence(core.getProperty(part, 'Name'), scene.getParent(part))
		scene.setLocalMatrix(newParent, scene.getLocalMatrix(part))
		for child in children:
			scene.setParent(child, newParent)
core.popProgression()
core.configureInterfaceLogger(True, True, True)
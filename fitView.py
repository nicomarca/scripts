
def center(pt1, pt2):
	return geom.Point3((pt1.x + pt2.x)/2, (pt1.y + pt2.y)/2, (pt1.z + pt2.z)/2)

def fitView(occurrences = [1], viewer = -1):
	aabb = scene.getAABB(occurrences)
	centerAABB = center(aabb.low, aabb.high)
	
	views = [[1, 0, 0, centerAABB.x], 
					[0, 0, -1, centerAABB.z],
					[0, 1, 0, centerAABB.y], 
					[0, 0, 0, 1]]
	
	proj = [[0, 0, 0, 0],
					[0, 0, 0, 0], 
					[0, 0, 0, aabb.high.y * 2], 
					[0, 0, 0, 1]]
	
	clipping = pxz.geom.Point2(0.19863334662738805, 1986.3334662738805)#(aabb.high.x, aabb.high.z)
	
	view.setViewerMatrices([views], [proj], clipping, viewer)

fitView()
from PyQt5.QtWidgets import QDialog, QVBoxLayout, QPushButton
from PyQt5.QtCore import *

class A(QObject):
	def __init__(self):
		QObject.__init__(self)

	def signal(self, action, name):
		# action['executeAction'].trigger()
		# algo.decimate([1], 0.500000, 0.100000, 1.000000, -1, False, 2)
		print(name)

class B(QObject):
	def __init__(self):
		QObject.__init__(self)
		ret = coregui.getFunctionWidget("algo", "tessellate", False)

		a = A()

		d = QDialog()
		l = QVBoxLayout(d)
		p = QPushButton("Execute")
		p.clicked.connect(d.close)
		p.clicked.connect(lambda: a.signal(ret, 'toto'))
		l.addWidget(ret['propertyWidget'])
		l.addWidget(p)
		coregui.applyApplicationStyle(d)

		d.exec()
	
	def slot(self):
		print('je suis dans mon slot')
	




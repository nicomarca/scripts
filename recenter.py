def vector(pt1, pt2):
	return Point3(pt2.x - pt1.x, pt2.y - pt1.y, pt2.z - pt1.z)
	
def dot_product(pt1, pt2):
	return pt1.x * pt2.x + pt1.y * pt2.y + pt1.z * pt2.z
	
def vector_length(v):
	return math.sqrt(dot_product(v, v))

def center(pt1, pt2):
	return Point3((pt1.x + pt2.x)/2, (pt1.y + pt2.y)/2, (pt1.z + pt2.z)/2)

def recenter_model(root):
	aabb = scene.getAABB([root])
	c = center(aabb.high, aabb.low)
	translationMatrix = [[1, 0, 0, -c.x], [0, 1, 0, -c.y], [0, 0, 1, -c.z], [0, 0, 0, 1]]
	scene.applyTransformation(root, translationMatrix)

root = 2
recenter_model(root)